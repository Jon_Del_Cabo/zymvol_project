from tkinter import *
import os
import subprocess
from subprocess import Popen, PIPE, STDOUT
from Backend.usuario import usuario
from Frontend.status_botones.desactivacion_botones import desactivar_botones_si_nada_seleccionado
from Frontend.actualizar import Update
from Frontend.verificacion.verificacion_montar import montar_error
from Frontend.trituradora import destruir
#from Frontend.funcion_botones.funcion_montar import mount

class MontarProyecto:
	def __init__(self, tabla_proyectos, btn_1, btn_2, btn_3):
		self.encriptar = Toplevel()
		self.encriptar.geometry("500x210")
		self.encriptar.minsize(width=500, height=210)
		self.encriptar.maxsize(width=500, height=210)
		self.encriptar.configure(bg='#D1D6D6')
		self.encriptar.title("Montar proyecto")
		
		def pedir_contrasena(updated_item, password_enc, passwd, dir_desencriptado, dir_encriptado):
			destruir(self.encriptar)
			self.solicitud_passwd = Toplevel()
			self.solicitud_passwd.geometry("300x210")
			self.solicitud_passwd.minsize(width=300, height=210)
			self.solicitud_passwd.maxsize(width=300, height=210)
			self.solicitud_passwd.configure(bg="#D1D6D6")
			
			def mount ():
				self.montar = subprocess.Popen(["encfs", "-S", dir_encriptado, dir_desencriptado], stdout=PIPE, stdin=PIPE)
				self.input_string = "{passwd}\n".format(passwd = password_enc.get())
				self.prueba24 = self.montar.communicate(input = self.input_string.encode("utf-8"))[0]
				self.prueba24
				destruir(self.solicitud_passwd)
				Update(tabla_proyectos)	
				montar_error(self, tabla_proyectos, updated_item, dir_desencriptado)
				desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)	
			
			self.lbl3 = Label(self.solicitud_passwd, text="Contraseña:", bg="#D1D6D6", fg="black", font=("Arial",18))
			self.lbl3.place(x=90, y=30)
			
			self.contraseña160 = Entry (self.solicitud_passwd, fg='black', textvariable=self.password_enc, bg="white", font=("Arial", 12))
			self.contraseña160.place(x=50, y=90, width=200)
			
			self.btn3 = Button(self.solicitud_passwd, text="Realizar", bg="black", fg="white", command=mount)#lambda:mount(self, dir_desencriptado, dir_encriptado, password_enc, passwd, updated_item, tabla_proyectos, btn_1, btn_2, btn_3))
			self.btn3.place(x=200, y=150)

		self.user = usuario()
		self.password_enc = StringVar()
		self.passwd = self.password_enc.get()
		self.directorio_base = os.path.join("/home", self.user, "Proyectos")
		self.seleccionado = tabla_proyectos.selection()	
		self.item = [tabla_proyectos.item(self.seleccionado)["text"]]
		self.updated_item = str(self.item)[2:-2]
		self.dir_desencriptado = os.path.join(self.directorio_base, self.updated_item)
		self.dir_encriptado = os.path.join(self.directorio_base, "." + self.updated_item + "_enc")
		
		self.texto_confirmacion = "¿Desea montar el proyecto " + self.updated_item + "?"
		self.confirmacion = Label(self.encriptar, bg="#D1D6D6", fg="black", font=("Arial",16), anchor="center", wraplength=400, text = self.texto_confirmacion)
		self.confirmacion.place(x=50, y=40, width=400)
		
		self.btn77 = Button(self.encriptar, text="Sí", bg="black", fg="white", command=lambda:pedir_contrasena(self.updated_item, self.password_enc, self.passwd, self.dir_desencriptado, self.dir_encriptado))
		
		self.btn77.place(x=280, y=120, width=75)

		self.btn78 = Button(self.encriptar, text="No", bg="black", fg="white", command=lambda:destruir(self.encriptar))
		self.btn78.place(x=140, y=120, width=75)
