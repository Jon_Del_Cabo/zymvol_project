from tkinter import *
from tkinter import ttk

def activar_botones (event, tabla_proyectos, btn_1, btn_2, btn_3):
#	Esto activa los botones de la interfaz cuando se seleccionado un elemtno de la tabla.	
	seleccion = tabla_proyectos.selection()
	seleccion_objeto_listado = [tabla_proyectos.item(seleccion)["text"]]
	seleccion_objeto_listado_v2 = str(seleccion_objeto_listado)
	var_vacia = str("[\'\']")
	if (seleccion_objeto_listado_v2 != var_vacia):
		btn_1["state"] = NORMAL
		btn_2["state"] = NORMAL
		btn_3["state"] = NORMAL
		btn_1.configure(bg="black", fg="white")
		btn_2.configure(bg="black", fg="white")
		btn_3.configure(bg="black", fg="white")
		print("Los botones se han activado.")
