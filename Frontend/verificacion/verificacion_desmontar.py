from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.trituradora import destruir

def desmontar_error(self, tabla_proyectos, updated_item, dir_desencriptado):
    self.status = 0   
        
    def mostrar_no_error_des(status):  
        if self.status != "Montado":
            self.desencriptar_funciona = Toplevel()
            self.desencriptar_funciona.geometry("500x150")
            self.desencriptar_funciona.minsize(width=500, height=150)
            self.desencriptar_funciona.maxsize(width=500, height=150)
            self.desencriptar_funciona.configure(bg="#D1D6D6")

            self.lbl178 = Label(self.desencriptar_funciona, text="¡El proyecto se ha desmontado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            self.lbl178.place(x=60, y=50)
            
            self.btn297 = Button(self.desencriptar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.desencriptar_funciona))
            self.btn297.place(x=410, y=100)
            
        else:      
            self.desencriptar_error = Toplevel()
            self.desencriptar_error.geometry("500x150")
            self.desencriptar_error.minsize(width=500, height=150)
            self.desencriptar_error.maxsize(width=500, height=150)
            self.desencriptar_error.configure(bg="#D1D6D6")
            self.lbl278 = Label(self.desencriptar_error, text="¡El proyecto no se ha podido desmontar!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            self.lbl278.place(x=60, y=50)  
            
            self.btn297 = Button(self.desencriptar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.desencriptar_error))
            self.btn297.place(x=410, y=100)
            
    def check_id_error_des(tabla_proyectos, updated_item, dir_desencriptado):
        self.user = usuario()
        self.directorio_base = os.path.join("/home", self.user, "Proyectos")
        self.carpetas = os.listdir(self.directorio_base)
        self.list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
        self.lista_mounted_dir = str(self.list_mounted_dir.communicate())
        if str(self.updated_item[-1]) in str(self.lista_mounted_dir):
            self.status = "Montado"
            mostrar_no_error_des(self.status)
        else:
            self.status = "Desmontado"
            mostrar_no_error_des(self.status)

    check_id_error_des(tabla_proyectos, updated_item, dir_desencriptado)
