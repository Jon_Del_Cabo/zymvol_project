from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.trituradora import destruir

def crear_error(self, tabla_proyectos, ID_proyecto, dir_desencriptado):
    self.status = 0     
      
    def mostrar_no_error_crear(status):  
        if status == "Montado":
            self.crear_funciona = Toplevel()
            self.crear_funciona.geometry("500x150")
            self.crear_funciona.minsize(width=500, height=150)
            self.crear_funciona.maxsize(width=500, height=150)
            self.crear_funciona.configure(bg="#D1D6D6")

            self.lbl178 = Label(self.crear_funciona, text="¡El proyecto se ha creado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            self.lbl178.place(x=60, y=50)
            
            self.btn297 = Button(self.crear_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.crear_funciona))
            self.btn297.place(x=410, y=100)
            
        else:      
            self.crear_error = Toplevel()
            self.crear_error.geometry("500x150")
            self.crear_error.minsize(width=500, height=150)
            self.crear_error.configure(bg="#D1D6D6")
            self.lbl278 = Label(self.crear_error, text="¡El proyecto no se ha podido crear!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            self.lbl278.place(x=60, y=50)  
            
            self.btn297 = Button(self.crear_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.crear_error))
            self.btn297.place(x=410, y=100)
            
    def check_id_error_crear(tabla_proyectos, ID_proyecto, dir_desencriptado):
        self.user = usuario()
        self.directorio_base = os.path.join("/home", self.user, "Proyectos")
        self.carpetas = os.listdir(self.directorio_base)
        self.list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
        self.lista_mounted_dir = str(self.list_mounted_dir.communicate())
        
        if str(ID_proyecto[-1]) in str(self.lista_mounted_dir):
            self.status = "Montado"
            mostrar_no_error_crear(self.status)
            
        else:
            self.status = "Desmontado"
            mostrar_no_error_crear(self.status)

    check_id_error_crear(tabla_proyectos, ID_proyecto, dir_desencriptado)
