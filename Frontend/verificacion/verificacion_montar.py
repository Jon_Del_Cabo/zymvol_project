from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.trituradora import destruir

def montar_error(self, tabla_proyectos, updated_item, dir_desencriptado):
	self.status = 0  
	def mostrar_no_error(status):  
		if status == "Montado":
			self.encriptar_funciona = Toplevel()
			self.encriptar_funciona.geometry("500x150")
			self.encriptar_funciona.minsize(width=500, height=150)
			self.encriptar_funciona.maxsize(width=500, height=150)
			self.encriptar_funciona.configure(bg="#D1D6D6")

			self.lbl178 = Label(self.encriptar_funciona, text="¡El proyecto se ha montado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
			self.lbl178.place(x=60, y=50)
			
			self.btn297 = Button(self.encriptar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.encriptar_funciona))
			self.btn297.place(x=410, y=100)
            
		else:      
			self.encriptar_error = Toplevel()
			self.encriptar_error.geometry("500x150")
			self.encriptar_error.minsize(width=500, height=150)
			self.encriptar_error.maxsize(width=500, height=150)
			self.encriptar_error.configure(bg="#D1D6D6")
			
			self.lbl278 = Label(self.encriptar_error, text="¡La contraseña no es correcta!", bg="#D1D6D6", fg="black", font=("Arial", 15))
			self.lbl278.place(x=60, y=50)
			
			self.btn297 = Button(self.encriptar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.encriptar_error))
			self.btn297.place(x=410, y=100)
            
	def check_id_error(tabla_proyectos, updated_item, dir_desencriptado):
		self.user = usuario()
		self.directorio_base = os.path.join("/home", self.user, "Proyectos")
		self.carpetas = os.listdir(self.directorio_base)
		self.list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
		self.lista_mounted_dir = str(self.list_mounted_dir.communicate())
		
		if str(self.updated_item[-1]) in str(self.lista_mounted_dir):
			self.status = "Montado"
			mostrar_no_error(self.status)
		else:
			self.status = "Desmontado"
			mostrar_no_error(self.status)
			
	check_id_error(tabla_proyectos, updated_item, dir_desencriptado)
