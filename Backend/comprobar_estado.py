from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from frontend.complemento import whoami, desactivar_botones_si_nada_seleccionado
from frontend.desencriptar import no_desmontado, si_desmontado
from frontend.montar import no_montado, si_montado

def status_carpeta(lista_mounted_dir, updated_item, tabla_proyectos, btn_1, btn_2, btn_3, btn_4):
    if updated_item in str(lista_mounted_dir):
        status_carpetas = "Montado"
    else:
        status_carpetas = "Desmontado"
    return status_carpetas
        
def conseguir_status(tabla_proyectos):
    user = whoami()
    directorio_base = os.path.join("/home", user, "Proyectos")
    list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
    lista_mounted_dir = str(list_mounted_dir.communicate())			
    seleccionado = tabla_proyectos.selection()	
    item = [tabla_proyectos.item(seleccionado)["text"]]
    updated_item = str(item)[2:-2]
    print (updated_item)
    return lista_mounted_dir, updated_item



def comprobar_estado_desmontar(tabla_proyectos, btn_1, btn_2, btn_3, btn_4):
    lista_mounted_dir, updated_item = conseguir_status(tabla_proyectos)
    status_carpetas = status_carpeta(lista_mounted_dir, updated_item, tabla_proyectos, btn_1, btn_2, btn_3, btn_4)
    if status_carpetas == "Montado":
        no_desmontado(tabla_proyectos, btn_1, btn_2, btn_3, btn_4)
    elif status_carpetas == "Desmontado":
        si_desmontado(tabla_proyectos, btn_1, btn_2, btn_3, btn_4)
    else:
        print("ha habido un error")
            

def comprobar_estado_montar(tabla_proyectos, btn_1, btn_2, btn_3, btn_4):
    lista_mounted_dir, updated_item = conseguir_status(tabla_proyectos)
    status_carpetas = status_carpeta(lista_mounted_dir, updated_item, tabla_proyectos, btn_1, btn_2, btn_3, btn_4)
    if status_carpetas == "Montado":
        si_montado(tabla_proyectos, btn_1, btn_2, btn_3, btn_4)
    elif status_carpetas == "Desmontado":
        no_montado(tabla_proyectos, btn_1, btn_2, btn_3, btn_4)
    else:
        print("ha habido un error")