from usuario import usuario
import os
import shutil
import subprocess
from subprocess import PIPE
import unittest

"""
Escribir en anacron : 1	30	backup		su - tu_usuario -c 'python3 direccion_completa_de_este_archivo' (esto hará que se ejecuta una vez al dia cada dia, tardará 30 minutos en proceder desde que el equipo se inicia, el nombre de la tarea será backup y lo siguiente es el comando

Después de LOGNAME escribir: START_HOURS_RANGE=3-22 (esto hará que si el equipo esta activo más de un día entero, entre el horario de 3 a.m hasta 22 p.m se podrá vovler a ejecutar el script, actua como cron más la ventaja de anacron)
"""
class Backup (object):
	def __init__(self):
		self.user = usuario()
		self.directorio_proyectos = os.path.join("/home", self.user, "Proyectos")
		self.directorio_raiz_backups = os.path.join("/home", self.user, "backups")
		self.listado_proyectos = os.listdir(self.directorio_proyectos)
		self.direcciones_montadas = []
	
	def backups (self):
		def comprobacion_montados(direcciones_montadas, proyecto, comprobacion_proyectos_montados_formato_texto):
			if self.direcciones_montadas[-1] in str(self.comprobacion_proyectos_montados_formato_texto):
				self.proyecto_comprimido = self.proyecto + ".zip"
				self.dir_backups_proyectos_comprimidos = os.path.join(self.directorio_raiz_backups, self.proyecto_comprimido)
				subprocess.call(["rm", "-rf", self.dir_backups_proyectos_comprimidos])
				shutil.make_archive(self.directorio_entero_backups, "zip", base_dir=self.directorio_entero_proyectos)
				
		for self.proyecto in self.listado_proyectos:
			if (self.proyecto[0] != "." and self.proyecto[-4:] != "_enc"):
				self.directorio_entero_proyectos = os.path.join(self.directorio_proyectos, self.proyecto)
				self.directorio_entero_backups = os.path.join(self.directorio_raiz_backups, self.proyecto)
				self.direcciones_montadas.append(self.directorio_entero_proyectos)
				self.comprobacion_proyectos_montados = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
				self.comprobacion_proyectos_montados_formato_texto = str(self.comprobacion_proyectos_montados.communicate())
				comprobacion_montados(self.direcciones_montadas, self.proyecto, self.comprobacion_proyectos_montados_formato_texto)
	
	def testing_backups (self, introducir_nombre_proyecto):
		def comprobacion_montados(introducir_nombre_proyecto, comprobacion_proyectos_montados_formato_texto):
			if self.direcciones_montadas[-1] in str(self.comprobacion_proyectos_montados_formato_texto):
				self.proyecto_comprimido = introducir_nombre_proyecto + ".zip"
				self.dir_backups_proyectos_comprimidos = os.path.join(self.directorio_raiz_backups, self.proyecto_comprimido)
				subprocess.call(["rm", "-rf", self.dir_backups_proyectos_comprimidos])
				self.direccion_creada = shutil.make_archive(self.directorio_entero_backups, "zip", base_dir=self.directorio_entero_proyectos)
			else:
				print("ERROR: El nombre que se ha solicitado no esta montado")
		if (introducir_nombre_proyecto[0] != "." and introducir_nombre_proyecto[-4:] != "_enc"):
			self.union = os.path.join(self.directorio_proyectos, introducir_nombre_proyecto)
			self.comprobacion = os.path.exists(self.union)
			if (self.comprobacion == True):
				self.direcciones_montadas.append(introducir_nombre_proyecto)
				self.directorio_entero_proyectos = os.path.join(self.directorio_proyectos, introducir_nombre_proyecto)
				self.directorio_entero_backups = os.path.join(self.directorio_raiz_backups, introducir_nombre_proyecto)
				self.comprobacion_proyectos_montados = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
				self.comprobacion_proyectos_montados_formato_texto = str(self.comprobacion_proyectos_montados.communicate())
				comprobacion_montados(introducir_nombre_proyecto, self.comprobacion_proyectos_montados_formato_texto)
				return self.direccion_creada
			else:
				print("ERROR: El nombre que se ha solicitado no existe el directorio Proyectos")
	
copia = Backup()
copia.backups()

"""
--- TESTING ---
Para comprobar que la copia de seguridad se realiza correctamente con el nombre que se ha introducido o se espera:
Requsitos:
	- El proyecto ha de existir.
	- El proyecto ha de estar montado.
	- Quítele las comillas a las líneas 86 y 87.
	
Como comprobar:
	- Introduzca el direcitorio que espera como resultado, es decir, la ruta del archivo .zip supuestamente creado en final_esperado.
	- Introduzca unicamente el nombre del archivo a testear, sin ninguna extensión, en final_creado.
	
En caso de error lea la primera linea de la excepción, en caso de no serle útil, lea el AssertionError.
"""
class Testing(unittest.TestCase):

	def testing(self):
		
		copia = Backup()
		final_esperado = "/home/jon/backups/Prueba24.zip"
		final_creado = copia.testing_backups("Prueba24")
		self.assertEqual(final_creado, final_esperado)

"""if __name__ == '__main__':
	unittest.main()"""
